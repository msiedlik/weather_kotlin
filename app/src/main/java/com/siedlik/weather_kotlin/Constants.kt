package com.siedlik.weather_kotlin

class Constants {

    companion object {
        const val BASE_URL = "http://api.openweathermap.org/"
        const val APPID = "749561a315b14523a8f5f1ef95e45864"

        const val SHARED_PREFERENCES = "shared preferences"
        const val KEY_Q = "q"
        const val KEY_UNITS = "units"
        const val ERROR_MSG = "error_msg"
        const val RETURN_MSG = "return_msg"
        const val INTENT_EXTRA_CITY_NAME = "city_name"
        const val INTENT_EXTRA_UNITS = "measure_system"
        const val UNITS_METRIC = "metric"
        const val UNITS_IMPERIAL = "imperial"

        const val ERROR_WRONG_CITY = R.string.error_wrong_city
        const val ERROR_TOO_MANY_REQUESTS = R.string.error_too_many_requests
        const val ERROR_UNEXPECTED = R.string.error_unexpected
    }
}