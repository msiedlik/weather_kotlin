package com.siedlik.weather_kotlin.service

import com.siedlik.weather_kotlin.Constants
import com.siedlik.weather_kotlin.model.WeatherData
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query
import java.util.*

interface OpenWeatherApi {

    @GET("data/2.5/weather")
    fun getWeatherData(
        @Query("q") q: String,
        @Query("APPID") APPID: String = Constants.APPID,
        @Query("units") units: String,
        @Query("lang") lang: String = Locale.getDefault().language
    ): Call<WeatherData>

}