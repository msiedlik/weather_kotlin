package com.siedlik.weather_kotlin.service

import com.siedlik.weather_kotlin.Constants
import com.siedlik.weather_kotlin.model.WeatherData
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class OpenWeatherService {

    companion object {
        fun getInstance(cityName: String, units: String): Call<WeatherData> {
            return Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(OpenWeatherApi::class.java)
                .getWeatherData(q = cityName, units = units)
        }
    }
}