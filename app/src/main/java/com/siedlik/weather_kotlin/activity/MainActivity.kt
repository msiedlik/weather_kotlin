package com.siedlik.weather_kotlin.activity

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.inputmethod.InputMethodManager
import com.siedlik.weather_kotlin.Constants
import com.siedlik.weather_kotlin.R
import com.siedlik.weather_kotlin.fragment.ErrorDialog
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private lateinit var textWatcher: TextWatcher

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        loadData()
        setContentView(R.layout.activity_main)
        initializeListeners()

        buttonCheck.setOnClickListener {
            val cityName = textCityName.text.toString()
            when {
                cityName.isNotEmpty() -> {
                    val units = if (radioGroupMain.checkedRadioButtonId == radioMetric.id) Constants.UNITS_METRIC else Constants.UNITS_IMPERIAL
                    saveData(cityName, units)
                    changeActivity(cityName, units)
                }
                else -> textLayoutCityName.error = getString(R.string.empty_field)
            }
        }
    }

    private fun saveData(q: String, units: String) {
        val sharedPreferences = getSharedPreferences(Constants.SHARED_PREFERENCES, MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.putString(Constants.KEY_Q, q)
        editor.putString(Constants.KEY_UNITS, units)
        editor.apply()
    }

    private fun loadData(){
        val sharedPreferences = getSharedPreferences(Constants.SHARED_PREFERENCES, MODE_PRIVATE)
        val q :String? = sharedPreferences.getString(Constants.KEY_Q, null)
        val units : String? = sharedPreferences.getString(Constants.KEY_UNITS, null)
        when {
            intent.hasExtra(Constants.ERROR_MSG) -> showErrorDialog(sharedPreferences)
            intent.hasExtra(Constants.RETURN_MSG) -> sharedPreferences.edit().clear().apply()
            (q != null && units != null) -> changeActivity(q, units)
        }
    }

    private fun showErrorDialog(preferences: SharedPreferences){
        preferences.edit().clear().apply()
        val error = intent.getStringExtra(Constants.ERROR_MSG)
        val errorDialog = ErrorDialog.newInstance(error)
        errorDialog.show(supportFragmentManager, "dialog")
    }

    private fun changeActivity(q: String, units: String) {
        val intent = Intent(this, WeatherActivity::class.java)
        intent.putExtra(Constants.INTENT_EXTRA_CITY_NAME, q)
        intent.putExtra(Constants.INTENT_EXTRA_UNITS, units)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent)
        finish()
    }

    private fun hideKeyboard(view: View) {
        val manager = view.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        manager.hideSoftInputFromWindow(view.windowToken, 0)
    }

    private fun initializeListeners(){
        main_layout.setOnTouchListener { v, _ ->
            hideKeyboard(v)
            true
        }

        textWatcher = object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun afterTextChanged(s: Editable) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                textLayoutCityName.error = null
            }
        }
        textCityName.addTextChangedListener(textWatcher)
    }

    override fun onStop() {
        super.onStop()
        textCityName?.removeTextChangedListener(textWatcher)
    }

}
