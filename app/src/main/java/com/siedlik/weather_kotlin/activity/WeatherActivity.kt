package com.siedlik.weather_kotlin.activity

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import android.view.View
import android.widget.PopupMenu
import com.siedlik.weather_kotlin.Constants
import com.siedlik.weather_kotlin.R
import com.siedlik.weather_kotlin.model.*
import com.siedlik.weather_kotlin.service.OpenWeatherService
import com.siedlik.weather_kotlin.toCamelCase
import com.siedlik.weather_kotlin.toUpperCaseAtFirst
import kotlinx.android.synthetic.main.activity_weather.*
import org.joda.time.DateTime
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.DecimalFormat


class WeatherActivity : AppCompatActivity() , PopupMenu.OnMenuItemClickListener {

    private lateinit var cityName: String
    private lateinit var units: String
    private lateinit var call: Call<WeatherData>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_weather)
        getIntentData()

        call = OpenWeatherService.getInstance(cityName, units)
        call.enqueue(object : Callback<WeatherData> {
            override fun onResponse(call: Call<WeatherData>, response: Response<WeatherData>) {
                when {
                    response.isSuccessful -> response.body()?.let { updateUI(it) }
                    response.code() == 404 -> goToMainActivity(Constants.ERROR_WRONG_CITY)
                    response.code() == 429 -> goToMainActivity(Constants.ERROR_TOO_MANY_REQUESTS)
                    else -> goToMainActivity(Constants.ERROR_UNEXPECTED)
                }
            }
            override fun onFailure(call: Call<WeatherData>, t: Throwable) = goToMainActivity(Constants.ERROR_UNEXPECTED)
        })

        initializeSwipeListener(call)
        initializeTimeIntervalUpdate(call, 5)
    }

    private fun goToMainActivity(error: Int? = null){
        val intent = Intent(this, MainActivity::class.java)
        when {
            error != null -> intent.putExtra(Constants.ERROR_MSG, getString(error))
            else -> intent.putExtra(Constants.RETURN_MSG, true)
        }
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent)
        finish()
    }

    private fun initializeSwipeListener(call: Call<WeatherData>) {
        swipeRefresh.setOnRefreshListener {
            updateWeatherData(call)
            swipeRefresh.isRefreshing = false
        }
    }

    private fun initializeTimeIntervalUpdate(call: Call<WeatherData>, minutes: Int) {
        val handler = Handler()
        object : Runnable {
            override fun run() {
                handler.postDelayed(this, 60000L * minutes)
                updateWeatherData(call)
            }
        }
    }

    private fun updateWeatherData(call: Call<WeatherData>) {
        call.clone().enqueue(object : Callback<WeatherData> {
            override fun onResponse(call: Call<WeatherData>, response: Response<WeatherData>) {
                when {
                    response.isSuccessful -> response.body()?.let { updateUI(it) }
                    else -> showSnackbar(getString(R.string.refresh_error))
                }
            }
            override fun onFailure(call: Call<WeatherData>, t: Throwable) = showSnackbar(getString(R.string.refresh_error))
        })
    }


    private fun updateUI(weatherData: WeatherData){
        setTimeData(weatherData.sys)
        setWeatherData(weatherData.weather[0], weatherData.sys)
        setTemperatureData(weatherData.main)
        setTimeData(weatherData.sys)
        setWindData(weatherData.wind)
        changeVisibility()
        showSnackbar(getString(R.string.updated))
    }

    private fun isDayTime(sys: Sys): Boolean {
        val sunriseTime = DateTime(Integer.parseInt(sys.sunrise) * 1000L)
        val sunsetTime = DateTime(Integer.parseInt(sys.sunset) * 1000L)
        val currentTime = DateTime.now()
        return currentTime.isAfter(sunriseTime) && currentTime.isBefore(sunsetTime)
    }

    private fun setTimeData(sys: Sys) {
        val sunriseTime = DateTime(Integer.parseInt(sys.sunrise) * 1000L)
        val sunsetTime = DateTime(Integer.parseInt(sys.sunset) * 1000L)
        val formatter = DecimalFormat("00")
        val sunriseText = "${sunriseTime.hourOfDay}:${formatter.format(sunriseTime.minuteOfHour)}"
        val sunsetText = "${sunsetTime.hourOfDay}:${formatter.format(sunsetTime.minuteOfHour)}"
        sunrise_textView.text = sunriseText
        sunset_textView.text = sunsetText
    }

    private fun setWeatherData(weather: Weather, sys: Sys) {
        val weatherID = Integer.parseInt(weather.id)
        val weatherText = weather.description.toUpperCaseAtFirst()
        val cityText = cityName.toCamelCase()
        if (cityText.length > 12) weather_text_CityName.textSize = 23F
        if (weatherText.length > 20) weather_current_textView.textSize = 16F
        setWeatherIcon(weatherID, isDayTime(sys))
        weather_text_CityName.text = cityText
        weather_current_textView.text = weatherText
    }

    private fun setWeatherIcon(weatherID: Int, day: Boolean) {
        var iconID: Int? = null
        when (weatherID) {
            in 200..299 -> iconID = (if (day) R.drawable.storm_d else R.drawable.storm_n)
            in 300..399 -> iconID = (R.drawable.raining)
            in 500..599 -> iconID = (if (day) R.drawable.rain_d else R.drawable.rain_n)
            in 600..699 -> iconID = (if (day) R.drawable.snow_d else R.drawable.snow_n)
            800 -> iconID = (if (day) R.drawable.clear else R.drawable.moon)
            in 801..899 -> iconID = (if (day) R.drawable.clouds_d else R.drawable.clouds_n)
        }
        iconID?.let { weather_weather_icon?.setImageResource(it) }
    }

    private fun setTemperatureData(main: Main) {
        val tempSystem = if (units == Constants.UNITS_METRIC) getString(R.string.C) else getString(R.string.F)
        val currentTemp = "${main.temp} $tempSystem"
        val maxTemp = "${main.temp_max} $tempSystem"
        val minTemp = "${main.temp_min} $tempSystem"
        current_temperature_textView.text = currentTemp
        max_temperature_textView.text = maxTemp
        min_temperature_textView.text = minTemp
    }

    private fun setWindData(wind: Wind?) {
        when {
            wind?.deg != null -> {
                wind_direction_imageView.rotation = wind.deg.toFloat()
                val windSpeed = "${wind.speed} ${getString(R.string.m_s)}"
                wind_textView.text = windSpeed
            }
            else -> {
                wind_title_textView.visibility = View.GONE
                wind_textView.visibility = View.GONE
                wind_direction_imageView.visibility = View.GONE
                weather_wind_error_icon.visibility = View.VISIBLE
                weather_wind_error_textView.visibility = View.VISIBLE
            }
        }
    }

    private fun changeVisibility(){
        weather_progressBar.visibility = View.GONE
        weather_up_layout.visibility = View.VISIBLE
        weather_down_layout.visibility = View.VISIBLE
    }

    private fun showSnackbar(message: String) = Snackbar
        .make(weather_layout, message, Snackbar.LENGTH_SHORT)
        .show()

    private fun getIntentData(){
        cityName = intent.getStringExtra(Constants.INTENT_EXTRA_CITY_NAME)
        units = intent.getStringExtra(Constants.INTENT_EXTRA_UNITS)
    }

    fun showMenu(v: View){
        val popupMenu = PopupMenu(this, v)
        popupMenu.setOnMenuItemClickListener(this)
        popupMenu.inflate(R.menu.weather_menu)
        popupMenu.show()
    }

    override fun onMenuItemClick(item: MenuItem?): Boolean {
        when {
            item?.itemId == R.id.weather_item_new_city -> {
                goToMainActivity()
                return true
            }
            item?.itemId == R.id.weather_item_refresh -> {
                updateWeatherData(call)
                return true
            }
        }
        return false
    }
}
