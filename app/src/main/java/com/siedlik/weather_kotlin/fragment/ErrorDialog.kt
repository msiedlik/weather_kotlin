package com.siedlik.weather_kotlin.fragment

import android.app.Dialog
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatDialogFragment
import com.siedlik.weather_kotlin.R

class ErrorDialog : AppCompatDialogFragment() {

    companion object {
        fun newInstance(message: String): ErrorDialog {
            val args = Bundle()
            args.putString("key", message)
            val fragment = ErrorDialog()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val bundle = this.arguments
        val message = bundle?.getString("key")!!
        val builder = AlertDialog.Builder(activity!!)
        builder.setTitle(R.string.error_occured)
            .setMessage(message)
            .setPositiveButton("OK") { _, _ ->  }
        return builder.create()
    }
}

