package com.siedlik.weather_kotlin.model

data class WeatherData(val dt: String, val visibility: String, val weather: Array<Weather>, val name: String,
                       val cod: String, val main: Main, val id: String, val sys: Sys, val base: String, val wind: Wind)

data class Main(val temp: String, val temp_min: String, val humidity: String, val pressure: String, val temp_max: String)

data class Sys(val country: String, val sunrise: String, val sunset: String, val id: String, val type: String, val message: String)

data class Weather(val icon: String, val description: String, val main: String, val id: String)

data class Wind(val deg: String, val speed: String)

