package com.siedlik.weather_kotlin

import org.apache.commons.text.WordUtils.*

fun String.toCamelCase(): String = capitalizeFully(this, ' ', '_')

fun String.toUpperCaseAtFirst(): String = this.substring(0, 1).toUpperCase() + this.substring(1)